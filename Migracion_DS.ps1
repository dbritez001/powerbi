$ErrorActionPreference = "SilentlyContinue"
#$sqlDatabaseServer = "cubosestadisticas-01"
$username = "britezd@reporteslink.com.ar"
$password = "Redlink*951" | ConvertTo-SecureString -asPlainText -Force 
$Credential = New-Object System.Management.Automation.PSCredential($username, $password)

Connect-PowerBIServiceAccount -Credential $Credential | Out-Null

function WorkSpace {

    $workspaces = (Get-PowerBIWorkspace -all).Name

    write-host "Del siguiente listado, elija el workspace sobre el que se encuentra el DataSet a migrar`n"

    foreach ($i in 1..$workspaces.count) {

        write-host "$i - $($workspaces[$i - 1])"

    }

    $opcion = $null

    do {
        $opcion = Read-Host "`nOpcion"
        $opcion = [int]$opcion  # Convertir la entrada a un número entero

        if ($opcion -ge 1 -and $opcion -le $workspaces.Count) {
            $ws = $workspaces[$opcion - 1]
        }
        else {
            Write-Host "Opción no válida. Ingrese un número válido."
        }
    } while ($opcion -lt 1 -or $opcion -gt $workspaces.Count)

    $workspaceid = (Get-PowerBIWorkspace -Name "$ws").id
	
    #    $dashboard = (Get-PowerBIDashboard -workspaceid $workspaceid).Name

    #    clear-host

    $wsvariables = @($ws, $workspaceid)
	
    return $wsvariables
	
}
function Dataset {

    param(
        [array]$function_ws
    )
    $ws = $function_ws[0]
    $workspaceid = $function_ws[1]

    write-host "`nLos Conjuntos de datos dentro de $ws son los siguientes. Elija el conjunto de datos a actualizar`n"
 
    $datasetlist = (Get-PowerBIDataset -workspaceid $workspaceid).Name

    write-host "Elija el conjunto de datos a actualizar`n"
    
    foreach ($i in 1..$datasetlist.count) {

        write-host "$i - $($datasetlist[$i - 1])"

    }
	
    $opcion = $null
	
    do {
        $opcion = Read-Host "`nOpcion"
        $opcion = [int]$opcion  # Convertir la entrada a un número entero

        if ($opcion -ge 1 -and $opcion -le $datasetlist.Count) {
            $ds = $datasetlist[$opcion - 1]
        }
        else {
            Write-Host "Opción no válida. Ingrese un número válido."
        }
    } while ($opcion -lt 1 -or $opcion -gt $datasetlist.Count)

    Write-Host "El DataSet seleccionado es $ds`n"
	
    $datasets = (Get-PowerBIDataset -Scope Organization -Name $ds).id
	
    $resultados = @()

    foreach ($id in $datasets) {
        $datasetid = $id
        $resultados += $datasetid
    
    }

    #    return $datasetid

    return $resultados

}
function take_control {
    param (
        [string]$workspaceid,
        [array]$datasetid
    )

    $groupId = $workspaceid  # El ID del grupo (workspace) que alberga el conjunto de datos.

    foreach ($dsid in $datasetid) {
        # Intenta vincularte a una nueva puerta de enlace
        try { 
            $uri = "groups/$groupId/datasets/$dsid/Default.TakeOver"
            Invoke-PowerBIRestMethod -Url $uri -Method Post
        }
        catch {
            if ($_.Exception.code -match "ItemNotFound") {
                Write-Host "Error: El conjunto de datos con ID $dsid no se encontró en el grupo $groupId."
            }
            else {
                Write-Host "Error inesperado: $($_.Exception.Message)"
            }
        }
    }
}
function update-datasource {

    param (
        [string]$workspaceid,
        [array]$datasetid
    )
    
    foreach ($dsid in $datasetid) {
        
        $datasource = Get-PowerBIDatasource -workspaceid $workspaceid -datasetid $dsid
        
        # parse REST to determine gateway Id and datasource Id

        $datasourceUrl = "groups/$workspaceId/datasets/$dsid/datasources"

        # execute REST call to determine gateway Id, datasource Id and current connection details
        $datasourcesResult = Invoke-PowerBIRestMethod -Method Get -Url $datasourceUrl | ConvertFrom-Json

        # parse REST URL used to patch datasource credentials
        $datasource = $datasourcesResult.value[0]
        $gatewayId = $datasource.gatewayId
        $datasourceId = $datasource.datasourceid
        $sqlDatabaseServerCurrent = $datasource.connectionDetails.server
        $sqlDatabaseNameCurrent = $datasource.connectionDetails.database

        # Construct url for update
        $datasourePatchUrl = "groups/$workspaceid/datasets/$dsid/Default.UpdateDatasources"

        # create HTTP request body to update datasource connection details
        $postBody = @{
            "updateDetails" = @(
                @{
                    "connectionDetails"  = @{
                        "server"   = "$sqlDatabaseServer"
                        "database" = "$sqlDatabaseNameCurrent"
                    }
                    "datasourceSelector" = @{
                        "datasourceType"    = "AnalysisServices"
                        "connectionDetails" = @{
                            "server"   = "$sqlDatabaseServerCurrent"
                            "database" = "$sqlDatabaseNameCurrent"
                        }
                        "gatewayId"         = "$gatewayId"
                        "datasourceId"      = "$datasourceId"
                    }
                })
        }

        $postBodyJson = ConvertTo-Json -InputObject $postBody -Depth 6 -Compress

        # convert body contents to JSON
        $postBodyJson = ConvertTo-Json -InputObject $postBody -Depth 6 -Compress

        # execute POST operation to update datasource connection details
        Invoke-PowerBIRestMethod -Method Post -Url $datasourePatchUrl -Body $postBodyJson

        # corroborar que se modifico el Dataset

        $datasourceUrlNew = "groups/$workspaceId/datasets/$dsid/datasources"

        $datasourcesResultNew = Invoke-PowerBIRestMethod -Method Get -Url $datasourceUrlNew | ConvertFrom-Json

        # parse REST URL used to patch datasource credentials
        # $datasourceNew = $datasourcesResultNew.value[0]
        # #$gatewayIdNew = $datasource.gatewayId
        # $datasourceIdNew = $datasource.datasourceid
        # $sqlDatabaseServerCurrentNew = $datasource.connectionDetails.server
        # $sqlDatabaseNameCurrentNew = $datasource.connectionDetails.database

        write-host ""

        write-host "Valores del Datasource antes del cambio"
        write-host "Datasource ID: "$datasource.datasourceid
        write-host "Datasource Servidor: "$datasource.connectionDetails.server
        write-host "Datasource Base de Datos: "$datasource.connectionDetails.database

        write-host ""

        write-host "Valores del Datasource luego del cambio"
        $datasourceNew = $datasourcesResultNew.value[0]
        write-host "Datasource ID: "$datasourceNew.datasourceid
        write-host "Datasource Servidor: "$datasourceNew.connectionDetails.server
        write-host "Datasource Base de Datos: "$datasourceNew.connectionDetails.database
    }
}

$flag = $true
$opt = $null

while ($flag) {

    write-host "Que desea realizar?"
    Write-Host "1. Migrar Dataset."
    Write-Host "2. Volver Dataset a Datasource original."

    $opt = read-Host "`nOpcion"
    $opt.GetType().Name

    if ($opt -eq 1) {
        $sqlDatabaseServer = "cubosestadisticas-01"
        $flag = $false
    }
    elseif ($opt -eq 2) {
        $sqlDatabaseServer = "lnk1234"
        $flag = $false
    }
    else {
        Write-Host "Opción no valida, intentelo nuevamente."
    }

}

$values_f1 = WorkSpace
$value_ds = Dataset -function_ws $values_f1
take_control -workspaceid $values_f1[1] -datasetid $value_ds
update-datasource -workspaceid $values_f1[1] -datasetid $value_ds
$gatewayidnew = "297f573e-54af-4f9c-8d36-2014e91d9b2b" 	# Gateway de prueba
$urigw1 = "https://api.powerbi.com/v1.0/myorg/gateways/$gatewayidnew/datasources"

# Realizas la solicitud REST y almacenas la respuesta en $response
$response = Invoke-PowerBIRestMethod -Method Get -Url $urigw1

# Conviertes la respuesta JSON en un objeto PowerShell
$jsonObject = $response | ConvertFrom-Json

# Recorres el objeto y muestras los valores en líneas separadas
$jsonObject.value | ForEach-Object {
    "Datasource Name: " + $_.datasourceName  # Reemplaza "NombreDelCampo1" por el nombre real del campo
    "Datasource ID: " + $_.id  # Reemplaza "NombreDelCampo2" por el nombre real del campo
    write-host ""
	# Agrega más líneas para otros campos si es necesario
}

    $sqlDatabaseServer = "LNK1234"
	$workspaceid = "3d74b89a-4d8d-4641-a16e-b71e6b208783"
	$datasetid = "44d3ff24-358e-4978-bb55-c961518ec432"			# DocumentosTest
	#$datasetid = "9a0fa41e-1941-47c2-8b37-75cbe9c5c049"		# DocumentosTest220520
	#$datasetid = "a09248ec-d472-4c75-9149-323f70b0515e"		# LNK4212_Documentos3
	#$datasetid = "64306025-b386-475c-8760-886180eaa043"		# LNK4212_Documentos
	
	
	foreach ($dsid in $datasetid) {
        
        $datasource = Get-PowerBIDatasource -workspaceid $workspaceid -datasetid $dsid
        
        # parse REST to determine gateway Id and datasource Id

        $datasourceUrl = "groups/$workspaceId/datasets/$dsid/datasources"

        # execute REST call to determine gateway Id, datasource Id and current connection details
        $datasourcesResult = Invoke-PowerBIRestMethod -Method Get -Url $datasourceUrl | ConvertFrom-Json

        # parse REST URL used to patch datasource credentials
        $datasource = $datasourcesResult.value[0]
        $gatewayId = $datasource.gatewayId
        $datasourceId = $datasource.datasourceid
        $sqlDatabaseServerCurrent = $datasource.connectionDetails.server
        $sqlDatabaseNameCurrent = $datasource.connectionDetails.database

        # Construct url for update
        $datasourePatchUrl = "groups/$workspaceid/datasets/$dsid/Default.UpdateDatasources"

        # create HTTP request body to update datasource connection details
        $postBody = @{
            "updateDetails" = @(
                @{
                    "connectionDetails"  = @{
                        "server"   = "$sqlDatabaseServer"
                        "database" = "$sqlDatabaseNameCurrent"
                    }
                    "datasourceSelector" = @{
                        "datasourceType"    = "AnalysisServices"
                        "connectionDetails" = @{
                            "server"   = "$sqlDatabaseServerCurrent"
                            "database" = "$sqlDatabaseNameCurrent"
                        }
                        "gatewayId"         = "$gatewayId"
                        "datasourceId"      = "$datasourceId"
                    }
                })
        }

        $postBodyJson = ConvertTo-Json -InputObject $postBody -Depth 6 -Compress

        # convert body contents to JSON
        $postBodyJson = ConvertTo-Json -InputObject $postBody -Depth 6 -Compress

        # execute POST operation to update datasource connection details
        Invoke-PowerBIRestMethod -Method Post -Url $datasourePatchUrl -Body $postBodyJson

        # corroborar que se modifico el Dataset

        $datasourceUrlNew = "groups/$workspaceId/datasets/$dsid/datasources"

        $datasourcesResultNew = Invoke-PowerBIRestMethod -Method Get -Url $datasourceUrlNew | ConvertFrom-Json

        # parse REST URL used to patch datasource credentials
        # $datasourceNew = $datasourcesResultNew.value[0]
        # #$gatewayIdNew = $datasource.gatewayId
        # $datasourceIdNew = $datasource.datasourceid
        # $sqlDatabaseServerCurrentNew = $datasource.connectionDetails.server
        # $sqlDatabaseNameCurrentNew = $datasource.connectionDetails.database

        write-host ""

        write-host "Valores del Datasource antes del cambio"
        write-host "Datasource ID: "$datasource.datasourceid
        write-host "Datasource Servidor: "$datasource.connectionDetails.server
        write-host "Datasource Base de Datos: "$datasource.connectionDetails.database

        write-host ""

        write-host "Valores del Datasource luego del cambio"
        $datasourceNew = $datasourcesResultNew.value[0]
        write-host "Datasource ID: "$datasourceNew.datasourceid
        write-host "Datasource Servidor: "$datasourceNew.connectionDetails.server
        write-host "Datasource Base de Datos: "$datasourceNew.connectionDetails.database
    }
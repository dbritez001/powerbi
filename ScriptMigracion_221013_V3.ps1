#1er paso 
# Fill these ###################################################
$workspaceID = "3d74b89a-4d8d-4641-a16e-b71e6b208783"
$datasetid = "dd9fc41f-a31c-4c48-a2be-8dcea679a38b"
$sqlDatabaseServer = "cubosestadisticas-01"
$username = "britezd@reporteslink.com.ar"
$password = "Redlink*951" | ConvertTo-SecureString -asPlainText -Force
################################################################

# Workstation: TestGatewaySSAS
# ID: 3d74b89a-4d8d-4641-a16e-b71e6b208783
# Dataset: dd9fc41f-a31c-4c48-a2be-8dcea679a38b
# Tablero: UsuDiario_Prueba_220318

# Nuevo Datasource
# Datasource Name: UsuDiario_SSAS2019
# Datasourte ID: 24459ccc-024b-494b-9e76-461b7f5710ee

#Import-Module MicrosoftPowerBIMgmt

Clear-Host
 
$Credential = New-Object System.Management.Automation.PSCredential($username, $password)

Connect-PowerBIServiceAccount -Credential $credential | Out-Null

#Login-PowerBIServiceAccount
####Before anything, make Admin
#$AdminEmail="user_regs_Feb2022_1@MOD130011.onmicrosoft.com"
#Invoke-PowerBIRestMethod -Method Post -Url "https://api.powerbi.com/v1.0/myorg/admin/groups/$workspaceID/users" -Body "{ 'emailAddress': '$AdminEmail','groupUserAccessRight': 'Admin'}"


$datasource = Get-PowerBIDatasource -WorkspaceId $workspaceID -DatasetId $datasetid

# Construct url
#$workspaceId = $workspaceID
#$datasetId = $dataset.Id
$datasourceUrl = "groups/$workspaceId/datasets/$datasetId/datasources"

# Call the REST API to get gateway Id, datasource Id and current connection details
$datasourcesResult = Invoke-PowerBIRestMethod -Method Get -Url $datasourceUrl | ConvertFrom-Json



#2do paso
#Elija el datasource id a modificar
  $d = 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
  foreach ($i in $d) {
    $i
    $datasourcesResult.value[$i]
  }


#3er paso
#---Set the value of the datsource to change--#
$ix = 0
# Parse the response
$datasource = $datasourcesResult.value[$ix]
$gatewayId = $datasource.gatewayId
$datasourceId = $datasource.datasourceId
$sqlDatabaseServerCurrent = $datasource.connectionDetails.server
$sqlDatabaseNameCurrent = $datasource.connectionDetails.database

# Construct url for update
$datasourePatchUrl = "groups/$workspaceId/datasets/$datasetId/Default.UpdateDatasources"

# create HTTP request body to update datasource connection details
$postBody = @{
  "updateDetails" = @(
    @{
      "connectionDetails"  = @{
        "server"   = "$sqlDatabaseServer"
        "database" = "$sqlDatabaseNameCurrent"
      }
      "datasourceSelector" = @{
        "datasourceType"    = "AnalysisServices"
        "connectionDetails" = @{
          "server"   = "$sqlDatabaseServerCurrent"
          "database" = "$sqlDatabaseNameCurrent"
        }
        #"gatewayId" = "$gatewayId"
        #"datasourceId" = "$datasourceId"
      }
    })
}

$postBodyJson = ConvertTo-Json -InputObject $postBody -Depth 6 -Compress

#Previous values
"Values before"
"WorkspaceID: " + $workspaceID
"DatasetID: " + $datasetid
"Datasource: " + $datasourcesResult.value[$ix].connectionDetails + "; DatasetSourceID: " + $datasourceId
"GatewayId: " + $gatewayId


# Execute take over of the dataset

#Invoke-PowerBIRestMethod -Method Post -Url https://api.powerbi.com/v1.0/myorg/groups/$workspaceID/datasets/$datasetid/Default.TakeOver


# Execute POST operation to update datasource connection details
Invoke-PowerBIRestMethod -Method Post -Url $datasourePatchUrl -Body $postBodyJson


# NOTE: dataset credentials must be reset after updating connection details


#Script para extraer lista de DataSource de un GateWay

#$gatewayidnew = "a8a844c0-575f-4f87-8f71-ee4c24232cd2" #el gateway de prueba
#$urigw1 = "https://api.powerbi.com/v1.0/myorg/gateways/$gatewayidnew/datasources"
#Invoke-PowerBIRestMethod -Method Get -Url $urigw1

#va a ser necesario en caso de que no quede asociado el GateWat al DataSource # mas que nada se va a usar para el ROLLBACK
# $gatewayidnew = "297f573e-54af-4f9c-8d36-2014e91d9b2b"  # ID gateway Cubos
$gatewayidnew = "a8a844c0-575f-4f87-8f71-ee4c24232cd2"

#debe cumplir con el nombre de la base de datos y el servidor del DataSource
$datasourceObjectIdsnew = "24459ccc-024b-494b-9e76-461b7f5710ee" #datasource de prueba "995baa59-6135-48ad-8a3d-34e9d211a40b" LNK1234 - "6e70c6b3-ff1f-4c8b-98e3-150205dc1f61" LNK4212
# create HTTP request body to update datasource connection details
$postBody10 = @{"gatewayObjectid" = "$gatewayidnew"
  "datasourceobjectids"           = @(
    "$datasourceObjectIdsnew")
}

$postBodyJsongw = ConvertTo-Json -InputObject $postBody10

$urigw = "https://api.powerbi.com/v1.0/myorg/groups/$workspaceID/datasets/$datasetid/Default.BindToGateway"
Invoke-PowerBIRestMethod -Method Post -Url $urigw -Body $postBodyJsongw